#!/usr/bin/env python3
import argparse as ap
import re
import yaml
import functools
import copy
from os import path, makedirs, sys
from multiprocessing import Pool
from hashlib import md5
import subprocess as sp

from pipeline_classes import *


input_name = re.compile(r"(.+)\..[^\.]*$")


# creates a final command string using
# a Cmd instance and an Env in which to run it
# tracks outputs for use as inputs to the next stage
# automatically creates error tracking files as needed
def build_commands(cmds_in, env, test):
    cmds = copy.deepcopy(cmds_in)
    last_output = None
    ret = []

    for cmd in cmds:
        if not cmd.input:
            cmd.input = last_output

        if cmd.output:
            cmd.output = cmd.output.format_map(env)
            last_output = cmd.output

        elif cmd.output_suffix:
            mtch = re.match(input_name, cmd.input)

            if mtch:
                name = mtch.group(1) + cmd.output_suffix
                cmd.output = name.format_map(env)
                last_output = cmd.output

            else:
                raise Exception("input for command" + str(cmd) + "has no extension")


        if cmd.err:
            cmd.err = cmd.err.format_map(env)

            errhead, errtail = path.split(cmd.err)
            if not test and errhead and not path.isdir(errhead):
                makedirs(errhead)

        if cmd.output:
            outhead, outtail = path.split(cmd.output)

            if not test and outhead and not path.isdir(outhead):
                makedirs(outhead)

            if not cmd.err:
                if not test:
                    if not path.isdir(path.join(outhead, "err/")):
                      makedirs(path.join(outhead, "err/"))
                    cmd.err = path.join(outhead, "err/", outtail + ".err")
                else:
                    cmd.err = path.join(outhead, "err/", outtail + ".err")


        ret.append((env.replace(cmd), cmd.pipe, cmd.output, cmd.err))

    return ret


# updates an Env instance with file contents
def update_env(filename, env):
    if filename:
        with open(filename, 'r') as fh:
            env.update(yaml.load(fh))


# runs a command via subprocess
def call(cmd, pipe, outfile, errfile):
    print(cmd, pipe, outfile, errfile, file=sys.stderr)
    if not errfile:
        m = md5()
        m.update(bytes(cmd, 'utf-8'))
        errfile = "cmd_" + m.hexdigest() + ".stderr"

    if pipe:
        with open(outfile, 'w') as out, open(errfile, 'w') as err:
            code = sp.call(cmd.split(), stdout=out, stderr=err)
            if code != 0:
                raise Exception()
    else:
        with open(errfile, 'w') as err:
            code = sp.call(cmd.split(), stderr=err)
            if code != 0:
                raise Exception()

# helper for running the pipeline items in sequence
def run(calls):
    for prog in calls:
        try:
            call(*prog)
        except:
            print("\nError running the following command:")
            print(*prog, file=sys.stderr)
            raise


# pretty print the test run
def test_print(commands):
    print()
    print("Commands Produced:")
    print()
    print("------------------------------------------------------------")
    for cmd in commands:
        for item in cmd:
            if item[3]:
              errf = item[3]
            else:
              errf = "None"
            print('"' + item[0] + '"', 'errfile: ' + errf, sep="\t")
            print()
        print("------------------------------------------------------------")


def main(args):
    # get commands
    with open(args.plan, 'r') as fh:
        plan = [Cmd(**item) for item in yaml.load(fh)]

    # get samples
    with open(args.clusters, 'r') as fh:
        clusters = yaml.load(fh)

    # Base environment for all commands
    env = Env({})
    for fi in args.reference:
        update_env(fi, env)

    # Create finished commands by looping over clusters and creating
    # specific environments for each cluster
    # then building the command in that updated environment
    commands = []
    for cluster in clusters:
        new_env = copy.copy(env)
        new_env.update(cluster)

        for item in ["out_dir", "err_dir"]:
          if item in env and not path.isdir(new_env[item]) and not args.test:
            makedirs(new_env[item])

        commands.append(build_commands(plan, new_env, args.test))

    if args.test:
        test_print(commands)

    else:
        # run the commands in order for each cluster
        pool = Pool(processes=args.cores)
        pool.map(run, commands)


parser = ap.ArgumentParser()
parser.add_argument('plan', type=str, default='default_plan.yml', help='plan file')
parser.add_argument('clusters', type=str, default='clusters.yml', help='data file descriptions')
parser.add_argument('reference', nargs='+', type=str, help="other yaml config files")
parser.add_argument('-n', '--cores', type=int, default=2, help='number of processes to use')
parser.add_argument('-t', '--test', action='store_true', help='test mode, outputs calls to be made')

if __name__ == '__main__':
    args = parser.parse_args()
    main(args)
