Recreating the BitBucket code repo for
>Lee CL, Mowery YM, Daniel AR, Zhang D, Sibley AB, Delaney JR, Wisdom AJ, Qin X, Wang X, Caraballo I, Gresham J, Luo L, Van Mater D, Owzar K, Kirsch DG. Mutational landscape in genetically engineered, carcinogen-induced, and radiation-induced mouse sarcoma. JCI Insight. 2019 Jul 11;4(13):e128698. doi: 10.1172/jci.insight.128698. PMID: 31112524; PMCID: PMC6629293.

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6629293/
